<?php

namespace App;

use App\Traits\ApiResponser;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use ApiResponser;
    
    const CREDIT = 'C';
    const DEBIT = 'D';
    const COMMISSION_WITHDRAW_CREDIT_CARD = .1;
    CONST COMMISSION_WITHDRAW_DEBIT_CARD = 0;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'nip',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function withdraw($amountWithdraw){

        switch($this->account_type){
            case self::CREDIT :
                $commission =  $amountWithdraw * self::COMMISSION_WITHDRAW_CREDIT_CARD;
            break;
            case self::DEBIT :
                $commission = $amountWithdraw * self::COMMISSION_WITHDRAW_DEBIT_CARD;
            break;
            default : 
            return $this->errorResponse("Error tipo de tarjeta", 404);
        }

        $balance = $this->amount_available;
        $balanceAfterWithdraw = $balance - $amountWithdraw;

        $this->amount_available = $balanceAfterWithdraw - $commission;

        if($this->amount_available >= 0)
        {
            if($this->save())
            return $this->succesResponse([
                    'amountWithdraw' => $amountWithdraw, 
                    'balanceAfterWithdraw' => $balanceAfterWithdraw,
                    'commissionWithdraw' => $commission,
                    'balanceAfterWithdrawAndCommission' => $this->amount_available,
                ], 200);
        }
        else{
            return $this->errorResponse("No cuenta con saldo suficiente", 404);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function withdraw(Request $request){
        $account = Account::where('account_number', $request->accountNumber)->first();

        $withdraw = $account->withdraw($request->amountWithdraw);
        return $withdraw;
    }
}


<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class AccountController extends ApiController
{
    public function index(){
        $accounts = Account::all();

        return $this->showAll($accounts);
    }

    public function show(Request $request)
    {
        $account = Account::with('user')->where('account_number',$request->account_number)->first();

        return $this->showOne($account);
    }
}

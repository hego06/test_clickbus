<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Account;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'birthdate' => $faker->date('Y-m-d', '2000,-01-01')
    ];
});

$factory->define(Account::class, function (Faker $faker) {
    return [
        'user_id' => User::inRandomOrder()->first()->id,
        'account_type' => $type = $faker->randomElement([Account::CREDIT, Account::DEBIT]),
        'account_number' => $faker->creditCardNumber,
        'expiration' => $faker->creditCardExpirationDate,
        'nip' => bcrypt('1234'),
        'amount_available' => $faker->numberBetween(0,10000),
        'credit_line' => $type  == Account::CREDIT ? 10000 : null, // if is a credit account has a credit line otrher wise it's null 
    ];
});

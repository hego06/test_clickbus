<?php

use App\User;
use App\Account;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $userQuantity = 5;
        $accountQuantity = 10;

        factory(User::class, $userQuantity)->create();
        factory(Account::class, $accountQuantity)->create();
    }
}
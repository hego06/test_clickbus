import Login from "./components/LoginComponent.vue";
import Account from "./components/AccountComponent.vue";


export const routes = [
    { path: "/", name: "login", component: Login},
    { path: "/account", name: "account", component: Account, props: true},
];
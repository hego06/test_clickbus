<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('cashMachine');
});

Route::get('account','AccountController@index')->name('account.index');
Route::post('account','AccountController@show')->name('account.show');

Route::post('withdraw/','TransactionController@withdraw')->name('transaction.withdraw');
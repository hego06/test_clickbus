## Installation manual

1. Clone the repository.
2. Execute **composer install** to dowload all the dependencies.
3. Create a data base on the localhost enviroment and configure the database connection on **.env** file. 
4. execute **composer dump-autoload**
5. execute migration **php artisan migrate**
6. execute seeder **php artisan db:seed**
7. serving laravel **php artisan serve**

<span style="color:red">Running the laravel server with artisan, by default, runs at **127.0.0.1:8000**.
if you run it in another environment maybe the path changes so you have to modify **resources\js\config.js** with your own hostname and execute **npm run**.
~~~~
export default {
	hostname: 'http://127.0.0.1:8000'
}
~~~~
##After modify config.js:
1. npm install
2. npm run dev